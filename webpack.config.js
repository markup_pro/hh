const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const ENV = process.env.APP_ENV;
const isProd = ENV === 'prod';

const config = {
  entry: __dirname + '/src/js/main.js',
  output: {
    path: __dirname + '/dist',
    filename: 'main.js',
    publicPath: ''
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: ['/node_modules/']
      },

      // html
      {
        test: /\.html/,
        loader: 'raw-loader'
      },

      // scss
      {
        test: /\.s?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
            },
          },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                  [
                    "autoprefixer",
                    {
                      // Options
                    },
                  ],
                ],
              },
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },

      // img
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              publicPath: '../',
            },
          },
          {
            loader: 'img-loader',
          },
        ],
      },

      // fonts
      {
        test: /\.(woff|woff2|eot|ttf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            },
          },
        ],
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanAfterEveryBuildPatterns: ['dist']
    }),

    new HtmlWebpackPlugin({
      template: __dirname + "/src/pages/index.html",
      inject: 'body'
    }),

    new MiniCssExtractPlugin({
      filename: 'css/[name].[hash:8].css',
    }),

    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.join(__dirname, 'src/fonts'),
          to: 'fonts/[name].[ext]',
        },
        {
          from: path.join(__dirname, 'src/img'),
          to: 'img'
        }
      ]
    }),
  ],
  devServer: {
    contentBase: './dist',
    overlay: true,
    open: true
  }
}

if (isProd) {
  config.plugins.push(
    new UglifyJSPlugin(),
  );
}

module.exports = config
